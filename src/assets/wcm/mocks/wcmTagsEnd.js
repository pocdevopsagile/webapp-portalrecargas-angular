wcm.listas.listaVideos = [
  {
    idVideo: "866316b1-7d7f-438b-b26c-c03781f627b6",
    idProductoDeCompra: "",
    idCategoriaDeCompra: "13",
    urlImagenCarrusel:
      "/wps/wcm/myconnect/8c61c6e4-6171-4761-8b05-35601b82daa8/380x220_jurassic.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-8c61c6e4-6171-4761-8b05-35601b82daa8-mQIlB9p",
    costo: "S/ 9.50",
    detalleVideo: {
      tiutlo: "Jurassic World: El reino caído",
      duracion: "129 minutos",
      descripcion:
        "Una joven que acaba de perder a su madre se hunde en la depresión. Pero una situación fortuita la acerca a Gibby, un mono capuchino que debe cuidar durante el verano. Su graciosa personalidad la ayudará a sanar.",
      descripcionMobile:
        "Owen y Claire regresan a las ruinas de Jurassic World para salvar a los dinosaurios del volcán.",
      genero: "Acción, Ciencia Ficción,Aventura,Thriller",
      elenco:
        "Dallas Howard, Bryce; Levine, Ted; Sermon, Isabella; Chaplin, Geraldine; Pineda, Daniella; Smith, Justice; Goldblum, Jeff; Jones, Toby; Wong, BD; Spall, Rafe; Cromwell, James; Pratt, Chris",
      elencoMobile: "Dallas Howard, Bryce; Levine, Ted",
      director: "Bayona, J.A.",
      tituloOriginal: "Jurassic World: Fallen Kingdom",
      urlBanner:
        "/wps/wcm/myconnect/f4093fcd-d43c-4920-a2a5-5eb9b3dc9378/290x370_jurassic.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-f4093fcd-d43c-4920-a2a5-5eb9b3dc9378-mQIlB9p",
      urlBannerMobile:
        "/wps/wcm/myconnect/670ba756-34f2-4a91-b6f3-6270c6abd82d/316x100_jurassic.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-670ba756-34f2-4a91-b6f3-6270c6abd82d-mQIlB9p",
    },
  },
  {
    idVideo: "f8aa09ff-7e3a-4cd4-8777-bc37c510a704",
    idProductoDeCompra: "",
    idCategoriaDeCompra: "13",
    urlImagenCarrusel:
      "/wps/wcm/myconnect/6500046d-c19e-4c38-84be-71ed95d899ae/380x220_transilvania.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-6500046d-c19e-4c38-84be-71ed95d899ae-mQIlB9p",
    costo: "S/ 9.50",
    detalleVideo: {
      tiutlo: "Hotel Transylvania 3: Monstruos de vacaciones",
      duracion: "97 minutos",
      descripcion:
        "La familia se va de crucero para que Drácula pueda tomarse vacaciones y abandonar por unos días su trabajo en el Hotel Transylvania. Todo va bien hasta que Mavis se da cuenta de que Drácula se enamoró de una humana que puede ponerlos en peligro.",
      descripcionMobile:
        "La familia se va de crucero para que Drácula descanse de su trabajo en el Hotel Transylvania.",
      genero: "Familar,Animación,Comedia",
      elenco:
        "Gaffigan, Jim; Hahn, Kathryn; James, Kevin; Sandler, Adam; Samberg, Andy; Key, Keegan-Michael; Shannon, Molly; Blinkoff, Ashe; Gomez, Selena; Spade, David; Drescher, Fran; Buscemi, Steve",
      elencoMobile: "Gaffigan, Jim; Hahn, Kathryn",
      director: "Tartakovsky, Genndy",
      tituloOriginal: "Hotel Transylvania 3: A Monster Vacation",
      urlBanner:
        "/wps/wcm/myconnect/182a5f92-1c29-43a9-8eee-ed6eb1a2d2d8/290x370_transil.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-182a5f92-1c29-43a9-8eee-ed6eb1a2d2d8-mQIlB9p",
      urlBannerMobile:
        "/wps/wcm/myconnect/3dc92d4a-b8b5-4b2c-9cea-369a584a86f2/316x100_transil.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-3dc92d4a-b8b5-4b2c-9cea-369a584a86f2-mQIlB9p",
    },
  },
  {
    idVideo: "ca591b9f-2d52-4a01-8685-48e229ebe0a2",
    idProductoDeCompra: "",
    idCategoriaDeCompra: "13",
    urlImagenCarrusel:
      "/wps/wcm/myconnect/90decfa1-da6a-432d-b2f7-17d78031d644/380x220_increible.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-90decfa1-da6a-432d-b2f7-17d78031d644-mQIlB9p",
    costo: "S/ 9.50",
    detalleVideo: {
      tiutlo: "Los increíbles 2",
      duracion: "118 minutos",
      descripcion:
        "Helen está en el centro de atención mientras Bob navega el heroísmo cotidiano de la vida normal en el hogar, cuando un nuevo villano trama una brillante y peligrosa estrategia que Los Increíbles solo pueden vencer juntos.",
      descripcionMobile:
        "Helen está en el centro de atención y Bob navega el heroísmo cotidiano de la vida normal.",
      genero: "Familiar,Aventura,Animación",
      elenco:
        "Jackson, Samuel L.; Nelson, Craig T.; Milner, Huck; Vowell, Sarah; Hunter, Holly",
      elencoMobile: "Jackson, Samuel L.; Nelson, Craig T.",
      director: "Bird, Brad",
      tituloOriginal: "Incredibles 2",
      urlBanner:
        "/wps/wcm/myconnect/b285c8f8-d150-467e-bfff-af0ffdc4e8d8/290x370_increible.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-b285c8f8-d150-467e-bfff-af0ffdc4e8d8-mQIlB9p",
      urlBannerMobile:
        "/wps/wcm/myconnect/708282ff-73c2-4378-8521-90ad765f2c43/316x100_increible.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-708282ff-73c2-4378-8521-90ad765f2c43-mQIlB9p",
    },
  },
  {
    idVideo: "7597ff5f-fe47-4d8d-9e9f-22647a61f850",
    idProductoDeCompra: "",
    idCategoriaDeCompra: "13",
    urlImagenCarrusel:
      "/wps/wcm/myconnect/58b20c06-a002-4780-b958-37c4a0715529/380x220_padre.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-58b20c06-a002-4780-b958-37c4a0715529-mQIlB9p",
    costo: "S/ 9.50",
    detalleVideo: {
      tiutlo: "The Padre",
      duracion: "95 minutos",
      descripcion:
        "Cuando un estafador es atrapado en medio de un nuevo atraco, escapa de la escena en un coche robado. En la carrera no advierte que una precoz niña de 16 años llamada Lena se está ocultando en el asiento trasero.",
      descripcionMobile:
        "Un estafador encontrará en su huida a un socio inesperado para su próximo golpe.",
      genero: "Drama",
      elenco:
        "Paquim, Marie; Hermida, Hailey; Nolte, Nick; Roth, Tim; Guzmán, Luis",
      elencoMobile: "Paquim, Marie; Hermida, Hailey;",
      director: "Sobol, Jonathan",
      tituloOriginal: "Padre (2018)",
      urlBanner:
        "/wps/wcm/myconnect/9c61783e-8065-4a1f-a37c-0aebb185d2ac/290x370_padre.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-9c61783e-8065-4a1f-a37c-0aebb185d2ac-mQIlB9p",
      urlBannerMobile:
        "/wps/wcm/myconnect/fc67e5c8-13ca-48d1-894d-bc9c44fad8ab/316x100_padre.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-fc67e5c8-13ca-48d1-894d-bc9c44fad8ab-mQIlB9p",
    },
  },
  {
    idVideo: "5b279d5d-96a9-42ee-8b98-729533edec43",
    idProductoDeCompra: "",
    idCategoriaDeCompra: "13",
    urlImagenCarrusel:
      "/wps/wcm/myconnect/e05eb76a-8866-4ff9-8d8d-64657077b9a6/380x220_terror.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-e05eb76a-8866-4ff9-8d8d-64657077b9a6-mQIlB9p",
    costo: "S/ 9.50",
    detalleVideo: {
      tiutlo: "Terror bajo la tierra 6",
      duracion: "98 minutos",
      descripcion:
        "Burt y Travis investigan una serie de ataques letales de gusanos gigantes. Burt sospecha que los graboides están siendo utilizados como armas en secreto y resulta envenenado. Tiene solo 48 horas para crear un antídoto.",
      descripcionMobile:
        "Un investigador es envenenado por un graboide y tiene 48 horas para encontrar un antídoto.",
      genero: "Acción,Ciencia Ficción,Horror",
      elenco:
        "Van Graan, Tanya; Kennedy, Jamie; Money, Jamie-Lee; Gross, Michael",
      elencoMobile: "Van Graan, Tanya; Kennedy, Jamie",
      director: "Paul, Don Michael",
      tituloOriginal: "Tremors: A Cold Day in Hell",
      urlBanner:
        "/wps/wcm/myconnect/1d74bc79-fe88-42bb-844d-62c7239e1424/290x370_terror.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-1d74bc79-fe88-42bb-844d-62c7239e1424-mQIlB9p",
      urlBannerMobile:
        "/wps/wcm/myconnect/cf470f64-3a9c-42b6-81b3-925f0d34528f/316x100_terror.jpg?MOD=AJPERES&ContentCache=NONE&CACHE=NONE&CACHEID=ROOTWORKSPACE-cf470f64-3a9c-42b6-81b3-925f0d34528f-mQIlB9p",
    },
  },
];
wcm.listas.listaGiftCards = [
  {
     "idGiftCard":"74a73606-00f9-4f31-91ab-a991bdb8bf45",
     "idProductoDeCompra":"182",
     "catvCodCategoria":"81",
     "nombreProducto":"Gift Card 10GB",
     "tipoMoneda":"S/ ",
     "costo":"90",
     "ordenOferta":"2.0",
     "recuerda":"Recuerda que solo aplica para líneas Prepago y Postpago (No ilimitados).",
     "urlCarrusel":"/assets/img/mocks/10GB.png",
     "tempCategory":"Regala GB para navegar"
  },
  {
     "idGiftCard":"e1cb0d75-11dd-41a4-8427-98a7552886b7",
     "idProductoDeCompra":"183",
     "catvCodCategoria":"81",
     "nombreProducto":"Gift Card 30GB",
     "tipoMoneda":"S/ ",
     "costo":"250",
     "ordenOferta":"3.0",
    "recuerda": "Recuerda que solo aplica para líneas Prepago y Postpago (No ilimitados).",
    "urlCarrusel":"/assets/img/mocks/30GB.png",
     "tempCategory":"Regala GB para navegar"
  },
  {
     "idGiftCard":"f68bdf2c-d175-4501-805d-98ef1bf8df27",
     "idProductoDeCompra":"188",
     "catvCodCategoria":"82",
     "nombreProducto":"Gift Card 30GB de Pack Video",
     "tipoMoneda":"S/ ",
     "costo":"200",
     "ordenOferta":"4.0",
    "recuerda": "Recuerda que aplica solo para líneas Prepago.",
    "urlCarrusel":"/assets/img/mocks/PackVideo.png",
     "tempCategory":"Regala Redes Sociales"
  },
  {
     "idGiftCard":"d3726683-c33e-4f44-adf4-1026a3ae7900",
     "idProductoDeCompra":"181",
     "catvCodCategoria":"81",
     "nombreProducto":"Gift Card 5GB",
     "tipoMoneda":"S/ ",
     "costo":"50",
     "ordenOferta":"1.0",
    "recuerda": "Recuerda que solo aplica para líneas Prepago y Postpago (No ilimitados).",
    "urlCarrusel":"/assets/img/mocks/5GB.png",
     "tempCategory":"Regala GB para navegar"
  },
  {
     "idGiftCard":"bd917578-1e94-4794-b38d-b6666f05ca0c",
     "idProductoDeCompra":"184",
     "catvCodCategoria":"82",
     "nombreProducto":"Gift Card Facebook Full 3",
     "tipoMoneda":"S/ ",
     "costo":"60",
     "ordenOferta":"3.0",
    "recuerda": "Recuerda que solo aplica para líneas Prepago.",
    "urlCarrusel":"/assets/img/mocks/Facebook3meses.png",
     
     "tempCategory":"Regala Redes Sociales"
  },
  {
     "idGiftCard":"fdf6abf1-867f-44fa-ba9d-84b1206bea3b",
     "idProductoDeCompra":"185",
     "catvCodCategoria":"82",
     "nombreProducto":"Gift Card Facebook Full 6",
     "tipoMoneda":"S/ ",
     "costo":"100",
     "ordenOferta":"5.0",
     "recuerda":"Recuerda que solo aplica para líneas Prepago.",
     "urlCarrusel":"/assets/img/mocks/Facebook6meses.png",
     "tempCategory":"Regala Redes Sociales"
  },
  {
     "idGiftCard":"798f0721-db95-4f63-9c5d-9ebb5a253527",
     "idProductoDeCompra":"187",
     "catvCodCategoria":"82",
     "nombreProducto":"Gift Card Instagram 3",
     "tipoMoneda":"S/ ",
     "costo":"60",
     "ordenOferta":"1.0",
    "recuerda": "Recuerda que solo aplica para líneas Prepago.",

    "urlCarrusel":"/assets/img/mocks/Instagram3meses.png",
     
     "tempCategory":"Regala Redes Sociales"
  },
  {
     "idGiftCard":"227817e3-f00d-4c29-9249-d7e478194a4c",
     "idProductoDeCompra":"186",
     "catvCodCategoria":"82",
     "nombreProducto":"Gift Card WhatsApp 12",
     "tipoMoneda":"S/ ",
     "costo":"50",
     "ordenOferta":"2.0",
    "recuerda": "Recuerda que aplica solo para líneas Prepago.",
    "urlCarrusel":"/assets/img/mocks/Whatsapp.png",
     "tempCategory":"Regala Redes Sociales"
  }
];
wcm.listas.listaWCMOpciones = [{ "idOpcion": "e7025848-cc65-4567-9100-dee251de6590", "DBProductCode": "CANJE-1", "DBCategoryCode": "16", "ordenOferta": "1.0", "optionsText": "1 Oportunidad", "pointsText": "10 Claro Puntos" }, { "idOpcion": "8d8f7e4c-c150-4247-865d-fc44b710a9ba", "DBProductCode": "CANJE-2", "DBCategoryCode": "16", "ordenOferta": "2.0", "optionsText": "2 Oportunidades", "pointsText": "20 Claro Puntos" }, { "idOpcion": "ab15451a-4924-48b6-96d4-ff88526e854a", "DBProductCode": "CANJE-3", "DBCategoryCode": "16", "ordenOferta": "3", "optionsText": "3 Oportunidades", "pointsText": "30 Claro Puntos" }, { "idOpcion": "9bb891ad-5773-469d-a471-6e248893087d", "DBProductCode": "CANJE-4", "DBCategoryCode": "16", "ordenOferta": "4", "optionsText": "4 Oportunidades", "pointsText": "40 Claro Puntos" }, { "idOpcion": "d390801d-3bff-4bbf-82f4-a0b2c1e50660", "DBProductCode": "CANJE-5", "DBCategoryCode": "16", "ordenOferta": "5.0", "optionsText": "5 Oportunidades", "pointsText": "50 Claro Puntos" }];
wcm.listas.listaWCMEvents = [
  {
    idEvent: "aaf58cd5-f57c-44df-9f60-64fa2810c0ad",
    subcategoryID: "16",
    ordenOferta: "1.0",
    LinkCondicionesSorteo:
      "http://www.drumsandco.com/Partitions/interpolevil.pdf",
    urlCarrusel: "assets/img/EventoDiegoFlores.png",
    urlNotificacion:
      "https://static.claro.com.pe/img/otros/Mi-Claro-Concurso-ZEN_1050x230.png",
    asuntoCorreo: "Te ganaste una opción para un Zen en concierto online",
    nombreEvento: "Zen - concierto online",
    fechaSorteo: "14/09/2020",
    fechaEvento: "18/09/2020",
    mostrarFechaSorteo: "1",
    textoNotificacion: "entradas",
  },
  {
    idEvent: "057da41a-0400-45e0-90f0-84d9d4a95202",
    subcategoryID: "17",
    ordenOferta: "2.0",
    LinkCondicionesSorteo:
    "http://www.drumsandco.com/Partitions/interpolevil.pdf",
    urlCarrusel: "assets/img/EventoDiegoFlores.png",
    urlNotificacion:
      "https://static.claro.com.pe/img/otros/img_header_mail_Claro_compras_1050x230.jpg",
    asuntoCorreo: "Juan Diego Flores",
    nombreEvento: "Juan Diego Flores",
    fechaSorteo: "10/04/2019",
    fechaEvento: "31/05/2019",
    mostrarFechaSorteo: "0",
    textoNotificacion: "cirugía destructiva al oído",
  },
  {
    idEvent: "668b9518-5246-4c8b-8b48-f5f23122fdc5",
    subcategoryID: "20",
    ordenOferta: "3.0",
    LinkCondicionesSorteo:
    "http://www.drumsandco.com/Partitions/interpolevil.pdf",
    urlCarrusel: "assets/img/EventoDiegoFlores.png",
    urlNotificacion:
      "https://static.claro.com.pe/img/otros/img_header_mail_Claro_compras_1050x230.jpg",
    asuntoCorreo: "Bon Jovi en concierto",
    nombreEvento: "Bon Jovi en Lima",
    fechaSorteo: "18/04/2019",
    fechaEvento: "16/05/2019",
    mostrarFechaSorteo: "",
    textoNotificacion: "tickets para concierto",
  },
  {
    idEvent: "5eee95ec-99d9-4c64-99ae-30ca4ea64c77",
    subcategoryID: "19",
    ordenOferta: "4.0",
    LinkCondicionesSorteo:
    "http://www.drumsandco.com/Partitions/interpolevil.pdf",
    urlCarrusel: "assets/img/EventoDiegoFlores.png", 
    urlNotificacion:
      "https://static.claro.com.pe/img/otros/img_header_mail_Claro_compras_1050x230.jpg",
    asuntoCorreo: "Twice from Wales",
    nombreEvento: "The Joy Formidable",
    fechaSorteo: "18/04/2019",
    fechaEvento: "31/05/2019",
    mostrarFechaSorteo: "1",
    textoNotificacion: "boletos",
  },
  {
    idEvent: "267f6b9f-1fdf-4420-b66c-59f613559b3d",
    subcategoryID: "20",
    ordenOferta: "5.0",
    LinkCondicionesSorteo:
    "http://www.drumsandco.com/Partitions/interpolevil.pdf",
    urlCarrusel: "assets/img/EventoDiegoFlores.png",
    urlNotificacion:
      "https://static.claro.com.pe/img/otros/img_header_mail_Claro_compras_1050x230.jpg",
    asuntoCorreo: "A Perfect Circle Lima",
    nombreEvento: "Primera vez APC llegan",
    fechaSorteo: "18/04/2019",
    fechaEvento: "23/05/2019",
    mostrarFechaSorteo: "0",
    textoNotificacion: "una foto autografiada",
  },
];
wcm.listas.listaWCMRecomendaciones =
  [
    {
      "idPromocion": "97e8d20c-4ba2-41c1-8ae8-ebe08446a07c",
      "Imagen": "/assets/img/promo4.jpg",
      "FechaHoraInicio": "01/01/2020 00:00",
      "FechaHoraFin": "31/12/2022 00:00",
      "codigoPlanTarifario": "2441",
      "TipoLinea": "1",
      "codigoPlanComercial": "",
      "Encendido": "1",
      "VecesDia": "100",
      "TipoFiltro": "1",
      "Prioridad": "1",
      "CallToAction": "1",
      "idProductoDeCompra": "101",
      "codigoCategoria": "13",
      "canales": "1 Portal de Compras,3 Mi Claro App,8 Recargas,23 Mi Claro App Recargas"
    },
    {
      "idPromocion": "11e8d20c-4ba2-41c1-8ae8-ebe08446a07c",
      "Imagen": "/assets/img/promo4.png",
      "FechaHoraInicio": "01/01/2020 00:00",
      "FechaHoraFin": "31/12/2022 00:00",
      "codigoPlanTarifario": "2441,2442",
      "TipoLinea": "2",
      "codigoPlanComercial": "",
      "Encendido": "1",
      "VecesDia": "100",
      "TipoFiltro": "2",
      "Prioridad": "1",
      "CallToAction": "1",
      "idProductoDeCompra": "161",
      "codigoCategoria": "10",
      "canales": "1 Portal de Compras,3 Mi Claro App,8 Recargas,23 Mi Claro App Recargas"
    },
    {
      "idPromocion": "12233c-4ba2-41c1-8ae8-ebe08446a07c",
      "Imagen": "/assets/img/promo1.jpg",
      "FechaHoraInicio": "01/01/2020 00:00",
      "FechaHoraFin": "31/03/2020 00:00",
      "codigoPlanTarifario": "2441",
      "TipoLinea": "2",
      "codigoPlanComercial": "",
      "Encendido": "1",
      "VecesDia": "1",
      "TipoFiltro": "2",
      "Prioridad": "3",
      "CallToAction": "1",
      "idProductoDeCompra": "",
      "codigoCategoria": "",
      "canales": "1 Portal de Compras,3 Mi Claro App,8 Recargas,23 Mi Claro App Recargas"
    }
  ];

wcm.listas.configWCMMusica = 
[
  {
     "id":"cfe1e774-4d8c-4a75-b811-2ccb69c043b9",
     "titulo":"Producto Mensual Postpago",
     "imgBannerMobile":"assets/img/mocks/claromusica/mensual/bannerMobile.jpg",
     "imgBannerDesktop":"assets/img/mocks/claromusica/mensual/bannerDesktop.jpg",
     "imgDetailMobile":"assets/img/mocks/claromusica/mensual/detalleMobile.jpg",
     "imgDetailDesktop":"assets/img/mocks/claromusica/mensual/detalleDesktop.jpg",
     "identificadorBD":"POS_MUSICA_MES001",
     "days":"31",
     "detailFirstParagraph":"Escucha la música que tú quieras, sin restricciones, sin conexión de internet. Disfruta de todas las funcionalidades que te ofrece Claro música.",
     "detailFeatures":"Playlists, álbumes y DJ´s sin consumir tus megas|Millones de canciones y una variedad de radios nacionales e internacionales|Reproduce tus canciones en cualquier lugar y sin anuncios|Pídelo cuando quieras y por el tiempo que desees",
     "detailLastParagraph":"¡Sólo suscríbete al plan ilimitado!",
     "linkClaroMusica":"https://www.claro.com.pe/personas/app/claro-musica/",
     "linkClaroMusicaLabel":"www.claro.com.pe/claromusica"
  },
  {
     "id":"7323ac56-293c-481a-873a-6d1839d97580",
     "titulo":"Producto Mensual Prepago",
     "imgBannerMobile":"assets/img/mocks/claromusica/mensual/bannerMobile.jpg",
     "imgBannerDesktop":"assets/img/mocks/claromusica/mensual/bannerDesktop.jpg",
     "imgDetailMobile":"assets/img/mocks/claromusica/mensual/detalleMobile.jpg",
     "imgDetailDesktop":"assets/img/mocks/claromusica/mensual/detalleDesktop.jpg",
     "identificadorBD":"PRE_MUSICA_MES001",
     "days":"31",
     "detailFirstParagraph":"Escucha la música que tú quieras, sin restricciones, sin conexión de internet. Disfruta de todas las funcionalidades que te ofrece Claro música.",
     "detailFeatures":"Playlists, álbumes y DJ´s sin consumir tus megas|Millones de canciones y una variedad de radios nacionales e internacionales|Reproduce tus canciones en cualquier lugar y sin anuncios|Pídelo cuando quieras y por el tiempo que desees",
     "detailLastParagraph":"¡Sólo suscríbete al plan ilimitado!",
     "linkClaroMusica":"https://www.claro.com.pe/personas/app/claro-musica/",
     "linkClaroMusicaLabel":"www.claro.com.pe/claromusica"
  },
  {
     "id":"cb7f6745-113c-40e8-b536-5d34c7fb6b64",
    "titulo": "Producto Semanal Postpago",
     
    "imgBannerMobile":"assets/img/mocks/claromusica/semanal/bannerMobile.jpg",
     "imgBannerDesktop":"assets/img/mocks/claromusica/semanal/bannerDesktop.jpg",
     "imgDetailMobile":"assets/img/mocks/claromusica/semanal/detalleMobile.jpg",
    "imgDetailDesktop": "assets/img/mocks/claromusica/semanal/detalleDesktop.jpg",
     
     "identificadorBD":"POS_MUSICA_SEM001",
     "days":"7",
     "detailFirstParagraph":"Escucha la música que tú quieras, sin restricciones, sin conexión de internet. Disfruta de todas las funcionalidades que te ofrece Claro música.",
     "detailFeatures":"Playlists, álbumes y DJ´s sin consumir tus megas|Millones de canciones y una variedad de radios nacionales e internacionales|Reproduce tus canciones en cualquier lugar y sin anuncios|Pídelo cuando quieras y por el tiempo que desees",
     "detailLastParagraph":"¡Sólo suscríbete al plan ilimitado!",
     "linkClaroMusica":"https://www.claro.com.pe/personas/app/claro-musica/",
     "linkClaroMusicaLabel":""
  },
  {
     "id":"df85b85c-dd81-44e0-bd11-fa68d8a213d9",
     "titulo":"Producto Semanal Prepago",

     "imgBannerMobile":"assets/img/mocks/claromusica/semanal/bannerMobile.jpg",
     "imgBannerDesktop":"assets/img/mocks/claromusica/semanal/bannerDesktop.jpg",
     "imgDetailMobile":"assets/img/mocks/claromusica/semanal/detalleMobile.jpg",
    "imgDetailDesktop": "assets/img/mocks/claromusica/semanal/detalleDesktop.jpg",

     "identificadorBD":"PRE_MUSICA_SEM001",
     "days":"7",
     "detailFirstParagraph":"Escucha la música que tú quieras, sin restricciones, sin conexión de internet. Disfruta de todas las funcionalidades que te ofrece Claro música.",
     "detailFeatures":"Playlists, álbumes y DJ´s sin consumir tus megas|Millones de canciones y una variedad de radios nacionales e internacionales|Reproduce tus canciones en cualquier lugar y sin anuncios|Pídelo cuando quieras y por el tiempo que desees",
     "detailLastParagraph":"¡Sólo suscríbete al plan ilimitado!",
     "linkClaroMusica":"https://www.claro.com.pe/personas/app/claro-musica/",
     "linkClaroMusicaLabel":"www.claro.com.pe/claromusica"
  }
];

wcm.listas.listaFondoRecomendados = [
  {
    idFondoRecomendado: "215931a9-8471-4f32-bf57-136a078ce455",
    titulo: "Chévere 1",
    fechaHoraInicio: "01/04/2021 00:00",
    fechaHoraFin: "31/12/2025 00:00",
    ImagenFondo:
    "assets/img/mocks/fondos-recomendados/chevere1.jpeg",
    identificador: "idProductoDeCompra",
    seleccion: "1",
    colorTexto: "white",
    prioridad: "1",
    activoFlag: "1",
  },
  {
    idFondoRecomendado: "763ed331-dac9-4cf4-a150-e50a29cf039b",
    titulo: "Chévere 2",
    fechaHoraInicio: "01/04/2021 00:00",
    fechaHoraFin: "31/12/2025 00:00",
    ImagenFondo:
    "assets/img/mocks/fondos-recomendados/X2-chev.png",
    identificador: "idProductoDeCompra",
    seleccion: "2",
    colorTexto: "white",
    prioridad: "1",
    activoFlag: "1",
  },
  {
    idFondoRecomendado: "65b062eb-3ef1-4b98-8fad-9bafc392e2b3",
    titulo: "Chévere 3",
    fechaHoraInicio: "01/04/2021 00:00",
    fechaHoraFin: "31/12/2025 00:00",
    ImagenFondo:
      "assets/img/mocks/fondos-recomendados/X2-chev.png",
    identificador: "idProductoDeCompra",
    seleccion: "3",
    colorTexto: "white",
    prioridad: "1",
    activoFlag: "1",
  },
  {
    idFondoRecomendado: "68184c3a-b3eb-48a2-b38f-57b4ecca8484",
    titulo: "Fondo para los productos triplica",
    fechaHoraInicio: "01/04/2021 00:00",
    fechaHoraFin: "31/12/2025 00:00",
    ImagenFondo:
    "assets/img/mocks/fondos-recomendados/recomenTriplica.jpeg",
    identificador: "idProductoDeCompra",
    seleccion: "121|122|301|141|356",
    colorTexto: "white",
    prioridad: "1",
    activoFlag: "1",
  },
  {
    idFondoRecomendado: "6e8330e1-9b5a-40ba-b252-152ba45db3a2",
    titulo: "Megas 12",
    fechaHoraInicio: "01/04/2021 00:00",
    fechaHoraFin: "31/12/2025 00:00",
    ImagenFondo:
      "assets/img/mocks/fondos-recomendados/megas12.png",
    identificador: "idProductoDeCompra",
    seleccion: "509",
    colorTexto: "white",
    prioridad: "1",
    activoFlag: "1",
  },
  {
    idFondoRecomendado: "278a41be-4f70-456d-a806-e3a1904391e7",
    titulo: "Megas 13",
    fechaHoraInicio: "01/04/2021 00:00",
    fechaHoraFin: "31/12/2025 00:00",
    ImagenFondo:
    "assets/img/mocks/fondos-recomendados/megas13.png",
    identificador: "idProductoDeCompra",
    seleccion: "510",
    colorTexto: "white",
    prioridad: "1",
    activoFlag: "1",
  },
  {
    idFondoRecomendado: "9aa6b59d-1a0c-45cc-9cfe-05b3c0617c11",
    titulo: "Promo postpagos",
    fechaHoraInicio: "19/03/2021 00:00",
    fechaHoraFin: "21/03/2021 00:00",
    ImagenFondo:
    "assets/img/mocks/fondos-recomendados/promos-postpago.jpeg",
    identificador: "idProductoDeCompra",
    seleccion: "9|10|282|283|861|862|863|881|882|864",
    colorTexto: "red",
    prioridad: "1",
    activoFlag: "1",
  },
  {
    idFondoRecomendado: "ca32d4de-a7c4-4c43-9ae2-2660f6ecf482",
    titulo: "Promo tik toks",
    fechaHoraInicio: "03/02/2021 00:00",
    fechaHoraFin: "31/05/2021 23:59",
    ImagenFondo:
    "assets/img/mocks/fondos-recomendados/tiktok.png",
    identificador: "idProductoDeCompra",
    seleccion: "6|921|922",
    colorTexto: "white",
    prioridad: "1",
    activoFlag: "1",
  },
];