function hereDocPortalCompras(f) { 
    return f.toString().replace(/^[^\/]+\/\*!?/, '').
    replace(/\*\/[^\/]+$/, '');
}
var wcm = {
    terms: {
        giftcard: {
            giftcard_5gb: '',
            giftcard_10gb: '',
            giftcard_30gb: '',
            giftcard_30gb_pack_video: '',
            giftcard_facebook: '',
            giftcard_instagram: '',
            giftcard_whatsapp: '',
        },
        canje_eventos: {
            evento_1: '',
            evento_2: '',
            evento_3: '',
            evento_4: '',
            evento_5: '',
        },
        cargo_recibo: ''
    },
    listas: {
        listaVideos: [],
     listaGiftCards: [],
     listaWCMOpciones: [],
     listaWCMEvents: [],
     listaWCMRecomendaciones: [],
     configWCMMusica: [],
     listaFondoRecomendados: []
     }
}
wcm.terms.giftcard.giftcard_5gb = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras GiftCard/terminos y condiciones/Paquete de 5GB"] */});
wcm.terms.giftcard.giftcard_10gb = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras GiftCard/terminos y condiciones/Paquete de 10GB"] */});
wcm.terms.giftcard.giftcard_30gb = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras GiftCard/terminos y condiciones/Paquete de 30GB"] */});
wcm.terms.giftcard.giftcard_30gb_pack_video = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras GiftCard/terminos y condiciones/Gift Card 30GB de Pack Video"] */});
wcm.terms.giftcard.giftcard_facebook = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras GiftCard/terminos y condiciones/Gift Card Facebook Full"] */});
wcm.terms.giftcard.giftcard_instagram = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras GiftCard/terminos y condiciones/Gift Card Instagram"] */});
wcm.terms.giftcard.giftcard_whatsapp = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras GiftCard/terminos y condiciones/Gift Card WhatsApp"] */});
wcm.terms.canje_eventos.evento_1 = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras canje de eventos content library/mas info/evento 1"] */});
wcm.terms.canje_eventos.evento_2 = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras canje de eventos content library/mas info/evento 2"] */});
wcm.terms.canje_eventos.evento_3 = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras canje de eventos content library/mas info/evento 3"] */});
wcm.terms.canje_eventos.evento_4 = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras canje de eventos content library/mas info/evento 4"] */});
wcm.terms.canje_eventos.evento_5 = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras canje de eventos content library/mas info/evento 5"] */});
wcm.terms.cargo_recibo = hereDocPortalCompras(function() {/*! [Element key="Body" type="content" context="selected" name="portal de compras/catalogo multiref/terminos y condiciones/compra con cargo en recibo"] */});