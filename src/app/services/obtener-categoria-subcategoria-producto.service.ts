import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { WefClientService } from '../core/http/wef-client.service';


@Injectable({
  providedIn: 'root'
})
export class ObtenerCategoriaSubcategoriaProductoService {

  public obtenerCategoriasXproductoResponse: any = {
    idRespuesta: null,
    categoria_padre :'',
    categoria_hija:'',
    errorService: false
  };


  constructor(private wcs: WefClientService) { }

  obtenerCategoriaYsubCategoriaXproducto(callback,id) {
    console.log("======================")
    console.log("iniciando servicio")
    const urlObtenerCategoria = environment.urlComprasyPAgosWef.obtenerCategoriasXproducto;
    const requestOfertaporCategoria = this.getrequestComprasyPagos(id);
    console.log("===========urlObtenerCategoria==============")
    console.log(urlObtenerCategoria)
    console.log("===========requestOfertaporCategoria==============")
    console.log(requestOfertaporCategoria)
    console.log('=========== Antes del consumo==============');
    this.wcs.post(urlObtenerCategoria, requestOfertaporCategoria).subscribe(
      response => {
        console.log(requestOfertaporCategoria)
        console.log('=========== durante del consumo==============');
        console.log(  response)
        
        this.obtenerCategoriasXproductoResponse.idRespuesta = response.comunResponseType.MessageResponse.Body.defaultServiceResponse.idRespuesta;
        if(response.comunResponseType.MessageResponse.Body.categoriasXproduto){
          this.obtenerCategoriasXproductoResponse.categoria_padre = response.comunResponseType.MessageResponse.Body.categoriasXproduto.cod_categoria_padre
          this.obtenerCategoriasXproductoResponse.categoria_hija = response.comunResponseType.MessageResponse.Body.categoriasXproduto.cod_categoria
        }
        
        console.log('=========== asignacion==============');
        console.log(this.obtenerCategoriasXproductoResponse)
        callback();
      },
      error => {
        console.log('Error obtenerCategoriaXproducto', error);
        this.obtenerCategoriasXproductoResponse.errorService = true
        callback();
      }
    );
  }

  getrequestComprasyPagos(id) {
    const request = {
      idProducto: id,

    };

    return request;
  }
}
