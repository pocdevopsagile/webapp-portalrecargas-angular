import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { WefClientService } from '../core/http/wef-client.service';

@Injectable({
  providedIn: 'root'
})
export class ObtenerCreditoSaldoProductoService {

  constructor(private wefClienteService: WefClientService, ) { }


  private responseDefaultValue = {
    idRespuesta: null,
    errorService : false,
    creditoSaldo: {
      lineaCredito: 0, lblSimboloLineaCredito: '',
      lblSaldoPrepago: '', lblSimboloMonedaSaldoPrep: ''
    }
  };

  public obtenerCreditoSaldoResponse: any = this.responseDefaultValue;

  resetObtenerCreditoSaldoResponse() {
    this.obtenerCreditoSaldoResponse = this.responseDefaultValue;
  }

  obtenerCreditoSaldoProducto(callback) {
    this.resetObtenerCreditoSaldoResponse();
    const urlObtenerCreditoSaldoProducto = environment.urlComprasyPAgosWef.obtenerCreditoSaldoProducto;
    this.wefClienteService.post(urlObtenerCreditoSaldoProducto, null)
    .subscribe(
      response => {
        const idRespuesta = response.comunResponseType.MessageResponse.Body.defaultServiceResponse.idRespuesta;
        const idRespuestaDP = response.comunResponseType.MessageResponse.Header.HeaderResponse.status.type;
        if (Number(idRespuestaDP) === 0 && Number(idRespuesta) === 0) {
          this.obtenerCreditoSaldoResponse.idRespuesta = idRespuesta;
          this.obtenerCreditoSaldoResponse.creditoSaldo.lblSimboloLineaCredito =
                    response.comunResponseType.MessageResponse.Body.simboloMonedaLineaDisp;
          this.obtenerCreditoSaldoResponse.creditoSaldo.lblSimboloMonedaSaldoPrep =
                    response.comunResponseType.MessageResponse.Body.simboloMonedaSaldoPrep;
          this.obtenerCreditoSaldoResponse.creditoSaldo.lineaCredito =
                    response.comunResponseType.MessageResponse.Body.lineaDisponible;
          this.obtenerCreditoSaldoResponse.creditoSaldo.lblSaldoPrepago =
                    response.comunResponseType.MessageResponse.Body.saldoPrepago;
        }
        console.groupEnd();
        callback();
        },
      error => {
        console.log('ObtenerCreditoSaldoProductoService > obtenerCreditoSaldoProducto error');
        console.log(error)
        this.responseDefaultValue.errorService = true
        callback();
        }
      );

  }



}
