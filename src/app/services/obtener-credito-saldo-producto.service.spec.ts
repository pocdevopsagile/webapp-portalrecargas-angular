import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { WefClientService } from '../core/http/wef-client.service';

import { ObtenerCreditoSaldoProductoService } from './obtener-credito-saldo-producto.service';
import { of, throwError } from 'rxjs';


describe('ObtenerCreditoSaldoProductoService', () => {
  const spy = jasmine.createSpyObj('WefClientService', ['post']);
  let wefClientSpy: jasmine.SpyObj<WefClientService>;
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ObtenerCreditoSaldoProductoService, { provide: WefClientService, useValue: spy }],
    imports: [HttpClientModule],
  }));

  let testData = {
    dpIdRespuesta: '0',
    idRespuesta: '0',
    saldoPrepago: 0,
    simboloMonedaLineaDisp: 'S./',
    simboloMonedaSaldoPrep: 'S./',
    lineaDisponible: '100',
  };
  function getMockData() {
    return  `{
      "comunResponseType": {
          "MessageResponse": {
              "Header": {
                  "HeaderResponse": {
                      "consumer": "",
                      "pid": "d293e793-1c01-4b3a-94b9-975c48886e8a",
                      "timestamp": "2019-10-02T16:50:45-05:00",
                      "VarArg": "",
                      "status": {
                          "type": "${testData.dpIdRespuesta}",
                          "code": "0",
                          "message": "EJECUCI?N CON ?XITO",
                          "msgid": "DPI01-4aed8382-6eea-43f2-9a49-efc974d895c3"
                      }
                  }
              },
              "Body": {
                  "defaultServiceResponse": {
                      "idRespuesta": "${testData.idRespuesta}",
                      "idTransaccional": "79ad7144-14fc-4c8a-a427-acb863061bb1",
                      "mensaje": "Consulta con ?xito, se proces? correctamente la solicitud."
                  },
                  "saldoPrepago": ${testData.saldoPrepago},
                  "simboloMonedaLineaDisp": "${testData.simboloMonedaLineaDisp}",
                  "simboloMonedaSaldoPrep": "${testData.simboloMonedaSaldoPrep}",
                  "lineaDisponible":"${testData.lineaDisponible}"
              }
          }
      }
    }`; }

  it('should retrieve backend data caso exitoso', () => {
    const service: ObtenerCreditoSaldoProductoService = TestBed.get(ObtenerCreditoSaldoProductoService);
    wefClientSpy = TestBed.get(WefClientService) as jasmine.SpyObj<WefClientService>;
    testData = {
      dpIdRespuesta: '0',
      idRespuesta: '0',
      saldoPrepago: 0,
      simboloMonedaLineaDisp: 'S./',
      simboloMonedaSaldoPrep: 'S./',
      lineaDisponible: '100',
    };
    wefClientSpy.post.and.returnValue(of(JSON.parse(getMockData())));
    expect(service).toBeTruthy();
    service.obtenerCreditoSaldoProducto(() => {
      expect(service.obtenerCreditoSaldoResponse.idRespuesta).toEqual(testData.idRespuesta);
      expect(service.obtenerCreditoSaldoResponse.creditoSaldo.lineaCredito).toEqual(testData.lineaDisponible);
      expect(service.obtenerCreditoSaldoResponse.creditoSaldo.lblSaldoPrepago).toEqual(testData.saldoPrepago);
      expect(service.obtenerCreditoSaldoResponse.creditoSaldo.lblSimboloLineaCredito).toEqual(testData.simboloMonedaLineaDisp);
      expect(service.obtenerCreditoSaldoResponse.creditoSaldo.lblSimboloMonedaSaldoPrep).toEqual(testData.simboloMonedaSaldoPrep);
    });
  });
  it('should retrieve backend data caso dpIRespuesta no exitoso', () => {
    const service: ObtenerCreditoSaldoProductoService = TestBed.get(ObtenerCreditoSaldoProductoService);
    wefClientSpy = TestBed.get(WefClientService) as jasmine.SpyObj<WefClientService>;

    testData = {
      dpIdRespuesta: '2',
      idRespuesta: '0',
      saldoPrepago: 0,
      simboloMonedaLineaDisp: 'S./',
      simboloMonedaSaldoPrep: 'S./',
      lineaDisponible: '100',
    };

    wefClientSpy.post.and.returnValue(of(JSON.parse(getMockData())));
    expect(service).toBeTruthy();
    service.obtenerCreditoSaldoProducto(() => {
      expect(service.obtenerCreditoSaldoResponse.idRespuesta).toBeNull();
      expect(service.obtenerCreditoSaldoResponse.creditoSaldo.lineaCredito).toEqual(0);
      expect(service.obtenerCreditoSaldoResponse.creditoSaldo.lblSaldoPrepago).toEqual('');
      expect(service.obtenerCreditoSaldoResponse.creditoSaldo.lblSimboloLineaCredito).toEqual('');
      expect(service.obtenerCreditoSaldoResponse.creditoSaldo.lblSimboloMonedaSaldoPrep).toEqual('');
    });
  });
  it('should retrieve backend data caso error 500', () => {
    const service: ObtenerCreditoSaldoProductoService = TestBed.get(ObtenerCreditoSaldoProductoService);
    wefClientSpy = TestBed.get(WefClientService) as jasmine.SpyObj<WefClientService>;
    wefClientSpy.post.and.returnValue(throwError({ status: 500 }));
    expect(service).toBeTruthy();
    service.obtenerCreditoSaldoProducto(() => {
      expect(service.obtenerCreditoSaldoResponse.idRespuesta).toBeNull();
      expect(service.obtenerCreditoSaldoResponse.creditoSaldo.lineaCredito).toEqual(0);
      expect(service.obtenerCreditoSaldoResponse.creditoSaldo.lblSaldoPrepago).toEqual('');
      expect(service.obtenerCreditoSaldoResponse.creditoSaldo.lblSimboloLineaCredito).toEqual('');
      expect(service.obtenerCreditoSaldoResponse.creditoSaldo.lblSimboloMonedaSaldoPrep).toEqual('');
    });
  });
});
