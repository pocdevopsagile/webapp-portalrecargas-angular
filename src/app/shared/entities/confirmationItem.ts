export class ConfirmationItem {
    label: string;
    value: string;
    styleClass: string;
    constructor(label: string, value: string, styleClass: string) {
        this.label = label;
        this.value = value;
        this.styleClass = styleClass;
    }
}
