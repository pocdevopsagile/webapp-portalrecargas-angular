export class ObtenerMetodosDePagoRequest {
  idProductoDeCompra;
  codTipoLinea;
  admin;
  constructor(idProductoDeCompra, codTipoLinea, admin) {
    this.idProductoDeCompra = idProductoDeCompra;
    this.codTipoLinea = codTipoLinea;
    this.admin = admin;
  }
}
