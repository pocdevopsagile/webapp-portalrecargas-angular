import { ObtenerMetodosPagoService } from './../../../services/obtener-metodos-pago.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Constantes } from 'src/app/services/constants';
import { ProductosFavoritosService } from 'src/app/services/productos-favoritos.service';
import { ProductosOfertaService } from 'src/app/services/productos-oferta.service';
import { ObtenerCategoriaService } from 'src/app/services/obtener-categoria.service';
import { MethodsService } from 'src/app/services/methods.service';


@Component({
  selector: 'app-subcategorias',
  templateUrl: './subcategorias.component.html',
  styleUrls: ['./subcategorias.component.scss'],
})
export class SubcategoriasComponent implements OnInit {
  @Input() categoriaEscogida: any;
  @Input() creditoSaldo: any;
  @Input() subcategoria: any; 
  @Input() idProductoElegido


  @Output() mostrarConfirma = new EventEmitter<boolean>();
  @Output() comparteProd = new EventEmitter<any>();
  @Output() comparteSubCat = new EventEmitter<any>();


  subEscogido: any;
  idSubEscogido: any;

  subtituloCategoria = '¿Qué tipo de paquete necesitas?';
  mostrarProductos = false;
  listaProductos = [];
  isLoading = false;
  mostrarMedioPagos = false;
  metodoPaso = '4';

  productoElegido = null;

  listaMetodosPago;
  obtenerMetodosPagoIdRespuesta: any;

  codigoCatOrSub = '';

  constructor(
    private methods: MethodsService,
    private productosOfertaService: ProductosOfertaService,
    private productosFavoritosService: ProductosFavoritosService,
    private omp: ObtenerMetodosPagoService,
    private ocs: ObtenerCategoriaService,
  ) {}

  ngOnInit() {
    console.log("==============SUBCATEGORIAS=========")
    console.log(this.categoriaEscogida)
    console.log(this.categoriaEscogida.codCategoria)
    if(this.subcategoria){
      this.escogerSub( this.subcategoria[0 ], this.encontrarPosicionSubcategoria(this.subcategoria))
    }
    
    this.codigoCatOrSub = this.categoriaEscogida.codCategoria;
    if (this.categoriaEscogida.codCategoria === '80') {
      this.metodoPaso = '5';
    }
  }

  

  escogerSub(sub: any, num: any) {
    console.log("==================")
    console.log(sub)
    console.log(num)
    this.ocs.categoriaSeleccionada.nestedSubcategory = null;

    if (this.subEscogido !== undefined) {
      document
        .getElementById('sub_' + this.idSubEscogido)
        .classList.remove('color_' + this.categoriaEscogida.codCategoria);
    }

    this.idSubEscogido = num;
    this.subEscogido = sub;
    this.ocs.categoriaSeleccionada.nestedSubcategory = sub;
    this.comparteSubCat.emit(this.subEscogido);
    this.codigoCatOrSub = this.subEscogido.idCategoriaDeCompra;

    setTimeout(() => {
        document
        .getElementById('sub_' + num)
        .classList.add('color_' + this.categoriaEscogida.codCategoria);
    }, 100);

    this.isLoading = true;
    this.mostrarProductos = false;
    this.mostrarMedioPagos = false;

    setTimeout(() => {
      this.obtenerProductos(sub);
      this.productosFavoritosService.flujoFav('N');
    }, 3000);
  }

  obtenerProductos(categoriaSeleccionada) {
    console.log("==============BUSCANDO PRODUCTO DE SUBCATEGORIA=========")
    console.log(categoriaSeleccionada)
    if (
      categoriaSeleccionada.codCategoria ===
        Constantes.PC_CATEGORIAS_COMPRA.gifCard1 ||
      categoriaSeleccionada.codCategoria ===
        Constantes.PC_CATEGORIAS_COMPRA.gifCard2
    ) {
      this.productosOfertaService
        .obtenerOfertas(categoriaSeleccionada)
        .subscribe((productos) => {
          console.log('obteniendo productos giftcard');
          this.listaProductos = productos;
          this.isLoading = false;
          this.mostrarProductos = true;
          setTimeout(() => {
            this.methods.scroll(document.getElementById('prod_bo'));
          }, 200);
        });
    } else {
      this.productosOfertaService
        .obtenerOfertas(categoriaSeleccionada)
        .subscribe((productos) => {
          console.log('obteniendo productos genericos');
          this.listaProductos = productos;
          this.isLoading = false;
          this.mostrarProductos = true;
          setTimeout(() => {
            this.methods.scroll(document.getElementById('prod_bo'));
          }, 200);
        });
    }
  }

  encontrarPosicionSubcategoria(sub){
      for (const [i, value] of this.categoriaEscogida.listaSubCategorias.entries()) {
        if( value.codCategoria === sub[0].codCategoria){
          return i
        }
    }
  }

  habilitarMetodoPago(arg: boolean) {
    this.mostrarMedioPagos = false;
    if (this.productoElegido) {
      console.log("==============producto elegido=========")
    console.log(this.productoElegido)
      this.omp
        .obtenerMetodosDePago(this.productoElegido)
        .subscribe((response) => {
          console.log("==============lista de metodos de pago=========")
          console.log(response.listMetodoPago)
          this.listaMetodosPago = response.listMetodoPago;
          this.obtenerMetodosPagoIdRespuesta = response.idRespuesta;

          this.mostrarMedioPagos = arg;
          setTimeout(() => {
            if (this.categoriaEscogida.codCategoria === '80') {
              this.methods.scroll(document.getElementById('gift-card-info'));
            } else {
              this.methods.scroll(document.getElementById('metodosDiv'));
            }
          }, 250);

        });
    } else {
      this.mostrarMedioPagos = arg;
    }
  }
  compartirProdElegido(productoElegido: any) {
    console.log('productoElegido Sub:', productoElegido);
    this.productoElegido = productoElegido;
    this.comparteProd.emit(productoElegido);
  }

  habilitarConfirmacion(estado: boolean) {
    this.mostrarConfirma.emit(estado);
  }
}
