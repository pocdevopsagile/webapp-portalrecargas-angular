import { PopupService } from 'src/app/services/popup.service';
import { ObtenerCategoriaService } from 'src/app/services/obtener-categoria.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProductosOfertaService } from 'src/app/services/productos-oferta.service';
import { Constantes } from '../../services/constants';
import { ServicioCompartidoService } from 'src/app/core/services/servicio-compartido.service';
import { MethodsService } from 'src/app/services/methods.service';

@Component({
  selector: 'app-recargas',
  templateUrl: './recargas.component.html',
  styleUrls: ['./recargas.component.scss']
})
export class RecargasComponent implements OnInit {

  @Input() activarRecargas: boolean;
  @Input() listaCategorias: any[];

  productoEscogido: any;
  idProdEscogido: any;
  listaProductos = [];

  @Output() abrirFlujoCarouselEvent = new EventEmitter<string>();
  @Output() productoRecargaSelectionEvent = new EventEmitter<any>();
  montosMock = [
    {
      monto: '3.00',
      vigencia: '1'
    },
    {
      monto: '5.00',
      vigencia: '30'
    },
    {
      monto: '10.00',
      vigencia: '30'
    },
    {
      monto: '15.00',
      vigencia: '30'
    },
    {
      monto: '20.00',
      vigencia: '30'
    },
    {
      monto: '50.00',
      vigencia: '30'
    }
  ];

  constructor(private productosOfertaService: ProductosOfertaService,
              public servicioCompartido: ServicioCompartidoService,
              private methods: MethodsService,
              private ocs: ObtenerCategoriaService, private popupService: PopupService) { }

  ngOnInit() {

    if (this.listaCategorias.length > 0 && this.activarRecargas) {
      this.obtenerProductos(this.listaCategorias[0]);
    }


    this.popupService.currentActionMessage.subscribe(message => {
      if (message.indexOf('action_recargas_') > -1) {
          console.groupCollapsed('Procesando call to action recargas');
          try {
            const parts = message.split('_');
            const catCod = parts[3];
            const idProd = parts[2];
            const filteredCat = this.listaCategorias.filter(cat => cat.codCategoria == catCod);
            const filteredProd = this.listaProductos.filter(prod => prod.idProductoDeCompra == idProd);
            if (filteredCat.length > 0 && filteredProd.length > 0) {
              const calledProd = filteredProd[0];
              this.escogerCard(calledProd);
              console.log('Call to action seleccionando producto %O', calledProd);
            } else {
              console.log('No existe producto para el call to action');
            }
            this.popupService.changeActionMessage('');
          } catch (error) {
            console.error('Ocurrio error', error);
            this.popupService.changeActionMessage('');
        }
          console.groupEnd();
        }
    });
  }

  escogerCard(productoSeleccionado: any) {
    this.ocs.categoriaSeleccionada = null;
    if (this.productoEscogido !== undefined) {
      document.getElementById('prod_' + this.idProdEscogido).style.border = 'solid 1px rgba(0, 0, 0, 0.03)';
    }

    (document.getElementById('inputMonto') as HTMLInputElement).value = '';
    document.getElementById('minMaxMonto').style.color = '#212121';
    document.getElementById('minMaxMonto').innerHTML = 'Monto minímo: S/ 3.00 ';
    document.getElementById('input-paquete').style.border = 'solid 1px rgba(0, 0, 0, 0.03)';
    this.idProdEscogido = productoSeleccionado.idProductoDeCompra;
    this.productoEscogido = productoSeleccionado;
    document.getElementById('prod_' + productoSeleccionado.idProductoDeCompra).style.border = 'solid 1px #2197ae';

    this.ocs.categoriaSeleccionada = this.listaCategorias[0];

    this.abrirFlujoCarouselEvent.emit('recargas');

    this.productoRecargaSelectionEvent.emit(this.productoEscogido);
    setTimeout(() => {
      this.methods.scroll(document.getElementById('metodosDiv'));
    }, 500);

  }

  onInput(event: any) {
    event.target.value = event.target.value.replace(/[^0-9]/g, '');
  }

  confirmarMonto() {

    if (this.listaProductos.length > 0) {

      const dynamicProduct = this.listaProductos.filter((elem) => {
        return Constantes.DYNAMIC_PRICE_RECHARGE_PRODUCT_CODES.indexOf(elem.codigoProducto) > -1;
      })[0];


      if (this.productoEscogido !== undefined) {
        document.getElementById('prod_' + this.idProdEscogido).style.border = 'solid 1px rgba(0, 0, 0, 0.03)';
      }

      document.getElementById('input-paquete').style.border = 'solid 1px #2197ae';

      const montoEs = (document.getElementById('inputMonto') as HTMLInputElement).value;

      const dynamicPrice = parseInt(montoEs, 10);
      if (dynamicProduct != null && dynamicPrice) {

        if (dynamicPrice > Constantes.RECHARGE_LIMITS.MAX) {
          document.getElementById('minMaxMonto').innerHTML = 'Ingrese un monto inferior a S/ 200.00 ';
          document.getElementById('minMaxMonto').style.color = '#da272c';
          this.abrirFlujoCarouselEvent.emit('');
          return;
        }
        if (dynamicPrice < Constantes.RECHARGE_LIMITS.MIN) {
          document.getElementById('minMaxMonto').innerHTML = 'Ingrese un monto superior a S/ 3.00 ';
          document.getElementById('minMaxMonto').style.color = '#da272c';
          this.abrirFlujoCarouselEvent.emit('');
          return;
        }

        document.getElementById('minMaxMonto').innerHTML = 'Monto minímo: S/ 3.00 ';
        document.getElementById('minMaxMonto').style.color = '#212121';
        dynamicProduct.precioMoneda = dynamicPrice;
        dynamicProduct.dynamicProductFlux = true;
        //  Reglas: Vigencia de recargas
        // 👉🏻Prepago:
        // De 3 a s/59: 30 días
        // De S/60 a más: 60 días
        // 👉🏻Postpago: 1 año
        // prepago
        if (this.servicioCompartido.tipoLinea == '1') {
          if (dynamicPrice >= 3 && dynamicPrice < 60) {
            dynamicProduct.vigencia = 30;
          } else {
            dynamicProduct.vigencia = 60;
          }
        } else {
          dynamicProduct.vigencia = 365;
        }
        // $scope.dynamicProductFlux = true;
        this.abrirFlujoCarouselEvent.emit('recargas');
        this.productoRecargaSelectionEvent.emit(dynamicProduct);

        setTimeout(() => {
          this.methods.scroll(document.getElementById('metodosDiv'));
        }, 1000);

      } else {
        this.abrirFlujoCarouselEvent.emit('recargas');
        document.getElementById('minMaxMonto').style.color = '#212121';
      }

    }


  }

  obtenerProductos(categoriaSeleccionada: any) {
    this.productosOfertaService.obtenerOfertas(categoriaSeleccionada).toPromise().then((response: any) => {
      this.listaProductos = response;
      console.log('Producto RECARGA::' + this.listaProductos);
      this.activarRecargas = true;

    }).catch(err => {
      console.error('error', err);
    });
  }


}
